
library(tidyverse)
pa <- . %>% print(n = Inf)
sc <- function(df, var_name) {
    df %>% select(contains(var_name))
}

df <- read_csv("hackathon.csv") %>% 
    mutate(isbn_13 = as.character(isbn_13))
df %>% write_rds("hackathon.rds")
