
library(jsonlite)
library(tidyverse)

build_query <- function(isbn){
    paste0("https://www.googleapis.com/books/v1/volumes?q=isbn:", isbn,
           "&key=", Sys.getenv("GOOGLE_BOOKS_API_KEY"))
}

parse_json <- . %>% fromJSON(simplifyDataFrame = FALSE)

q <- build_query('9780062304346')
valid <- parse_json(q)
# # result <- fromJSON(q, simplifyDataFrame = FALSE)
# 
valid$totalItems
valid$items[[1]]
valid$items[[1]]$selfLink
valid$items[[1]]$volumeInfo$title
valid$items[[1]]$volumeInfo$authors
valid$items[[1]]$volumeInfo$publishedDate
valid$items[[1]]$volumeInfo$description
valid$items[[1]]$volumeInfo$industryIdentifiers[[1]]
valid$items[[1]]$volumeInfo$industryIdentifiers[[2]]$type
valid$items[[1]]$volumeInfo$industryIdentifiers[[2]]$identifier
valid$items[[1]]$volumeInfo$pageCount
valid$items[[1]]$volumeInfo$averageRating
valid$items[[1]]$volumeInfo$ratingsCount
valid$items[[1]]$volumeInfo$imageLinks


invalid <- build_query('9780062304347') %>% parse_json()
invalid$totalItems

extract_info <- function(parsed_json){
    if(parsed_json$totalItems == 0){
        return(NULL)
    } else {
        first_item <- parsed_json$items[[1]]
        first_volume <- first_item$volumeInfo
        result <- tibble(
            self_link = first_item$selfLink,
            title = first_volume$title,
            authors = first_volume$authors %>% paste(collapse = ";"),
            published_date = first_volume$publishedDate,
            description = first_volume$description,
            type = first_volume$industryIdentifiers[[2]]$type,
            identifier = first_volume$industryIdentifiers[[2]]$identifier,
            page_count = first_volume$pageCount,
            average_rating = first_volume$averageRating,
            ratings_count = first_volume$ratingsCount)
        return(result)
    }
}

valid %>% extract_info()
invalid %>% extract_info()


isbn <- c('9780062304346', '9780062304347')

result <- tibble(isbn = isbn)

result2 <- 
    result %>% 
    mutate(payload = isbn %>% build_query() %>% map(. %>% parse_json() %>% extract_info()))


payload <- isbn %>% build_query() %>% map(. %>% parse_json() %>% extract_info())


isbn
payload <- isbn %>% build_query() %>% map(parse_json)

temp <- tibble(isbn, payload)


result2 <- 
    result %>% 
    mutate(payload = isbn %>% build_query() %>% map(parse_json))

