
library(tidyverse)

parse_json <- . %>% jsonlite::fromJSON(simplifyDataFrame = FALSE)

build_query <- function(isbn){
    paste0("https://www.googleapis.com/books/v1/volumes?q=isbn:", isbn,
          "&key=", Sys.getenv("GOOGLE_BOOKS_API_KEY"))
}


if_null <- function(x, value){
    if(is.null(x)) value else x
}

if_null_chr <- partial(if_null, value = NA_character_)
if_null_int <- partial(if_null, value = NA_integer_)

extract_info <- function(parsed_json){
    if(parsed_json$totalItems == 0){
        return(NULL)
    } else {
        first_item <- parsed_json$items[[1]]
        first_volume <- first_item$volumeInfo
        result <- tibble(
            self_link = first_item$selfLink,
            title = first_volume$title,
            authors = first_volume$authors %>% paste(collapse = ";"),
            published_date = first_volume$publishedDate,
            description = if_null_chr(first_volume$description),
            page_count = if_null_int(first_volume$pageCount),
            average_rating = if_null_int(first_volume$averageRating),
            ratings_count = if_null_int(first_volume$ratingsCount))
        return(result)
    }
}
