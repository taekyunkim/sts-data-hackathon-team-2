
library(tidyverse)
library(readxl)
library(stringr)

pa <- . %>% print(n = Inf)
sc <- function(df, var_name) {
    df %>% select(contains(var_name))
}

sales <- read_rds("04-combined.rds") %>% 
    mutate_at(vars(grade_3, grade_4, grade_5, grade_6, grade_7, grade_8), as.numeric)
# book <- read_rds("google_book_info.rds")

# # book ~ rating
# 
# df_book <- sales %>% 
#     group_by(isbn_13, title, rating) %>% 
#     summarise(revenue = )

school <- 
    sales %>% 
    group_by(name_building, ucn, grade_3, grade_4, grade_5, grade_6, grade_7, grade_8) %>% 
    summarise(revenue = sum(revenue)) %>% 
    ungroup()

school %>% ggplot(aes(grade_3, revenue)) + geom_point() + geom_smooth()
school %>% ggplot(aes(grade_4, revenue)) + geom_point() + geom_smooth()
school %>% ggplot(aes(grade_5, revenue)) + geom_point() + geom_smooth()
school %>% ggplot(aes(grade_6, revenue)) + geom_point() + geom_smooth()
school %>% ggplot(aes(grade_7, revenue)) + geom_point() + geom_smooth()
school %>% ggplot(aes(grade_8, revenue)) + geom_point() + geom_smooth()


school_revenue <- sales %>% 
    group_by(name_building, ucn) %>% 
    summarise(revenue = sum(revenue)) %>% 
    ungroup()

clean_name <- . %>% 
    str_trim() %>% 
    str_to_lower() %>% 
    str_replace_all(" ", "_") %>% 
    str_replace_all("-", "_")
clean_dfname <- . %>% set_names(clean_name)

score <- read_csv("3-8_ELA_AND_MATH_RESEARCHER_FILE_2017.csv") %>% 
    clean_dfname()

score_sub <- 
    score %>% 
    filter(item_subject_area == "ELA" & subgroup_code == "01") %>% 
    filter(!is.na(county_code)) %>% 
    filter(!is.na(nrc_code)) %>% 
    filter(county_desc %in% c("BROOME", "WESTCHESTER", "SUFFOLK", "NASSAU")) %>% 
    filter(nrc_code == 5)


school_score <- 
    score_sub %>%     
    mutate(grade_num = item_desc %>% str_extract("\\d") %>% as.numeric()) %>% 
    mutate(grade = paste0('grade_', grade_num)) %>% 
    select(name, nrc_code, nrc_desc, county_code, county_desc, grade, mean_scale_score) %>% 
    group_by(name, nrc_code, county_code) %>% 
    summarise(mean_score = mean_scale_score %>% as.numeric() %>% mean())

result <- school_revenue %>% 
    inner_join(school_score, by = c('name_building' = 'name'))


g <- lm(revenue ~ mean_score, result)
summary(g)

