
library(tidyverse)
library(stringr)

pa <- . %>% print(n = Inf)
sc <- function(df, var_name) {
    df %>% select(contains(var_name))
}

item_sales <- read_rds("item_sales.rds") 

top_item_sales <- item_sales %>% 
    top_n(80, revenue)

    
source("google_books.r")

# result <- 
#     top_item_sales$isbn_13 %>% 
#     build_query() %>% 
#     map(parse_json)
#     
# result %>% write_rds("google_books.rds")

result <- read_rds("google_books.rds")

parsed_info <-
    result %>%
    map(extract_info)

book_info <- tibble(
    isbn_13 = top_item_sales$isbn_13, 
    parsed_info = result %>% map(extract_info)) %>% 
    mutate(valid = parsed_info %>% map_lgl(is.data.frame)) %>% 
    filter(valid) %>% 
    unnest(parsed_info) %>% 
    select(-valid)

book_info %>% 
    replace_na(list(ratings_count = 0)) %>% 
    write_rds("google_book_info.rds")

