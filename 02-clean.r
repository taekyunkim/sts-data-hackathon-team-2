
library(tidyverse)
library(stringr)

pa <- . %>% print(n = Inf)
sc <- function(df, var_name) {
    df %>% select(contains(var_name))
}

df <- read_rds("hackathon.rds") 

df_ny <- df %>% filter(state_name == "new york")
df_ny %>% write_rds('ny_sales.rds')

item_sales <- df_ny %>% 
    group_by(isbn_13, title, property) %>% 
    summarise(
        revenue = sum(revenue),
        n_fair = n()) %>% 
    ungroup() %>% 
    mutate(rev_per_fair = revenue / n_fair)

item_sales %>% write_rds("item_sales.rds")
