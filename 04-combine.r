
library(tidyverse)
library(readxl)
library(stringr)

pa <- . %>% print(n = Inf)
sc <- function(df, var_name) {
    df %>% select(contains(var_name))
}

# item_sales <- read_rds("item_sales.rds") 
# top_item_sales <- item_sales %>% 
#     top_n(80, revenue)

ny_sales <- read_rds("ny_sales.rds")

clean_name <- . %>% 
    str_trim() %>% 
    str_to_lower() %>% 
    str_replace_all(" ", "_") %>% 
    str_replace_all("-", "_")
clean_dfname <- . %>% set_names(clean_name)

birth <- read_excel('zip_birth.xlsx') %>% 
    clean_dfname()

ny_sales_sub <- 
    ny_sales %>% 
    inner_join(birth, by = c('zipcode' = 'zip_code'))


score <- read_csv("3-8_ELA_AND_MATH_RESEARCHER_FILE_2017.csv") %>% 
    clean_dfname()

score_sub <- 
    score %>% 
    filter(item_subject_area == "ELA" & subgroup_code == "01") %>% 
    filter(!is.na(county_code)) %>% 
    filter(!is.na(nrc_code)) %>% 
    filter(county_desc %in% c("BROOME", "WESTCHESTER", "SUFFOLK", "NASSAU")) %>% 
    filter(nrc_code == 5)

score_wide <- 
    score_sub %>% 
    mutate(grade_num = item_desc %>% str_extract("\\d") %>% as.numeric()) %>% 
    mutate(grade = paste0('grade_', grade_num)) %>% 
    select(name, nrc_code, nrc_desc, county_code, county_desc, grade, mean_scale_score) %>% 
    spread(grade, mean_scale_score)

# score_wide %>% write_excel_csv("score_wide.csv")
# score_wide %>% write_rds("score_wide.rds")

df_combined <- 
    ny_sales_sub %>% 
    inner_join(score_wide, by = c('name_building' = 'name'))

df_combined %>% write_rds("04-combined.rds")

